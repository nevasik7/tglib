package tlogger

import (
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
	"time"
)

type Logger interface {
	Error(msg string)
	Tracef(msg string, args ...any)
	Debugf(msg string, args ...any)
	Infof(msg string, args ...any)
	Warnf(msg string, args ...any)
	Errorf(msg string, args ...any)
	Fatalf(msg string, args ...any)
	Panicf(msg string, args ...any)
}

func Init() Logger {
	zerolog.TimeFieldFormat = time.RFC3339Nano
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: "15:04:05"})

	once.Do(func() {
		zLog, consoleWriter := GetZeroLogger()

		newLogger := zLog.Output(consoleWriter)

		logger = &lg{zl: &newLogger, stopCh: make(chan struct{})}
	})

	return logger
}

// ----------INFO----------

func (lc *lg) realInfof(msg string, args ...any) {
	lc.wgLogs.Add(1)
	lc.zl.Trace().CallerSkipFrame(2).Msgf(msg, args...)
	lc.wgLogs.Done()
}

func (lc *lg) Infof(format string, args ...interface{}) {
	lc.realInfof(format, args)
}

func (lc *lg) Info(msg string) {
	lc.realInfof(msg)
}

// ----------ERROR----------

func (lc *lg) realErrorf(msg string, args ...interface{}) {
	lc.wgLogs.Add(1)
	lc.zl.Error().CallerSkipFrame(2).Err(errors.Errorf(msg, args...)).Send()
	lc.wgLogs.Done()
}

func (lc *lg) Errorf(format string, args ...interface{}) {
	lc.realErrorf(format, args...)
}

func (lc *lg) Error(msg string) {
	lc.realErrorf(msg)
}

// ----------TRACE----------

func (lc *lg) realTracef(msg string, args ...any) {
	lc.wgLogs.Add(1)
	lc.zl.Trace().CallerSkipFrame(2).Msgf(msg, args...)
	lc.wgLogs.Done()
}

func (lc *lg) Tracef(msg string, args ...any) {
	lc.realTracef(msg, args...)
}

func (lc *lg) Trace(msg string) {
	lc.realTracef(msg)
}

// ----------PANIC----------

func (lc *lg) realPanicf(msg string, args ...interface{}) {
	lc.wgLogs.Add(1)
	lc.zl.WithLevel(zerolog.PanicLevel).CallerSkipFrame(2).Err(errors.Errorf(msg, args...)).Send()

	lc.wgLogs.Wait()
	close(lc.stopCh)
	lc.wgFlush.Wait()
	panic(msg)
}

func (lc *lg) Panicf(format string, args ...any) {
	lc.realPanicf(format, args...)
}

func (lc *lg) Panic(msg string) {
	lc.realPanicf(msg)
}

// ----------DEBUG----------

func (lc *lg) realDebugf(msg string, args ...any) {
	lc.wgLogs.Add(1)
	lc.zl.Debug().CallerSkipFrame(2).Msgf(msg, args...)
	lc.wgLogs.Done()
}

func (lc *lg) Debugf(format string, args ...interface{}) {
	lc.realDebugf(format, args...)
}

func (lc *lg) Debug(msg string) {
	lc.realDebugf(msg)
}

// ----------FATAL----------

func (lc *lg) realFatalf(msg string, args ...interface{}) {
	lc.wgLogs.Add(1)
	lc.zl.WithLevel(zerolog.FatalLevel).CallerSkipFrame(2).Err(errors.Errorf(msg, args...)).Send()

	lc.wgLogs.Wait()
	close(lc.stopCh)
	lc.wgFlush.Wait()
	os.Exit(1)
}

func (lc *lg) Fatalf(format string, args ...interface{}) {
	lc.realFatalf(format, args...)
}

func (lc *lg) Fatal(msg string) {
	lc.realFatalf(msg)
}

// ----------WARN----------

func (lc *lg) realWarnf(msg string, args ...interface{}) {
	lc.wgLogs.Add(1)
	lc.zl.Warn().CallerSkipFrame(2).Err(errors.Errorf(msg, args...)).Send()
	lc.wgLogs.Done()
}

func (lc *lg) Warnf(format string, args ...interface{}) {
	lc.realWarnf(format, args...)
}

func (lc *lg) Warn(msg string) {
	lc.realWarnf(msg)
}

// ----------OTHERS----------

func Print(msg string) {
	logger.realDebugf(msg)
}

func Printf(format string, args ...any) {
	logger.realDebugf(format, args...)
}

func Println(msg string) {
	logger.realDebugf(msg)
}

func Trace(msg string) {
	logger.realTracef(msg)
}

func Debug(msg string) {
	logger.realDebugf(msg)
}

func Info(msg string) {
	logger.realInfof(msg)
}

func Warn(msg string) {
	logger.realWarnf(msg)
}

func Error(err error) {
	if err != nil {
		logger.realErrorf(err.Error())
	}
}

func Fatal(msg string) {
	logger.realFatalf(msg)
}

func Panic(msg string) {
	logger.realPanicf(msg)
}

func Tracef(format string, args ...any) {
	logger.realTracef(format, args...)
}

func Debugf(format string, args ...any) {
	logger.realDebugf(format, args...)
}

func Infof(format string, args ...any) {
	logger.realInfof(format, args...)
}

func Warnf(format string, args ...any) {
	logger.realWarnf(format, args...)
}

func Errorf(format string, args ...any) {
	logger.realErrorf(format, args...)
}

func Fatalf(format string, args ...any) {
	logger.realFatalf(format, args...)
}

func Panicf(format string, args ...any) {
	logger.realPanicf(format, args...)
}
