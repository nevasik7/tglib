package tlogger

import (
	"github.com/rs/zerolog"
	"sync"
)

var (
	chatIDRanx      = "-1002163038433"
	threadIdRanx    = "69"
	contentType     = "Content-Type"
	applicationJson = "application/json"
)

var (
	Ranx   string
	Plugin string
)

var (
	zl               *zerolog.Logger
	consoleWriterPtr *zerolog.ConsoleWriter
	onceL            sync.Once
	logger           *lg
	once             sync.Once
)
