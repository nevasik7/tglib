package tlogger

import (
	"github.com/rs/zerolog"
	"log"
	"os"
	"time"
)

func GetZeroLogger() (*zerolog.Logger, *zerolog.ConsoleWriter) {
	onceL.Do(func() {
		moscowLocation, err := time.LoadLocation("Europe/Moscow")
		if err != nil {
			log.Println("Failed to load Moscow timezone")
		}

		zerolog.ErrorStackMarshaler = GetMarshalStack(3)
		zerolog.TimeFieldFormat = time.StampMilli
		zerolog.ErrorFieldName = zerolog.MessageFieldName
		zerolog.TimestampFunc = func() time.Time {
			return time.Now().In(moscowLocation)
		}

		consoleWriterPtr = &zerolog.ConsoleWriter{Out: os.Stderr,
			TimeFormat:    time.TimeOnly,
			FieldsExclude: []string{"stack"},
			NoColor:       false,
		}

		zLogger := zerolog.New(consoleWriterPtr).
			With().
			Timestamp().
			Caller().
			Stack().
			Logger()

		zl = &zLogger
	})

	return zl, consoleWriterPtr
}
