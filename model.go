package tlogger

import (
	"github.com/rs/zerolog"
	"sync"
)

type telegramMessage struct {
	ChatID          string `json:"chat_id"`
	Text            string `json:"text"`
	MessageThreadID string `json:"message_thread_id,omitempty"`
}

type lg struct {
	zl   *zerolog.Logger
	logs *struct {
		logs chan []byte
	}
	wgLogs  sync.WaitGroup
	wgFlush sync.WaitGroup
	mu      sync.Mutex
	stopCh  chan struct{}
}

func (lc *lg) Write(p []byte) (n int, err error) {
	cp := make([]byte, len(p))
	copy(cp, p)

	lc.logs.logs <- cp
	return len(cp), nil
}
