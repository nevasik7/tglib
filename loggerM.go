package tlogger

type LoggerM struct {
}

func (l *LoggerM) Warnf(format string, v ...interface{}) {
	Warnf(format, v...)
}
func (l *LoggerM) Debugf(format string, v ...interface{}) {
	Debugf(format, v...)
}
func (l *LoggerM) Errorf(format string, v ...interface{}) {
	Errorf(format, v...)
}
