package tlogger

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func SendRTg(text string, tag string) error {
	var (
		message = telegramMessage{}
	)

	url := fmt.Sprintf("https://api.telegram.org/bot7359465015:AAH6-f61xPkIil_uzydXtFVbVvzxcu_zudo/sendMessage")

	switch tag {
	case Ranx:
		message = telegramMessage{
			ChatID:          chatIDRanx,
			Text:            text,
			MessageThreadID: threadIdRanx,
		}
	default:
		return fmt.Errorf("not found tag")
	}

	jsonData, err := json.Marshal(message)
	if err != nil {
		return fmt.Errorf("error marshalling message: %w", err)
	}

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(jsonData))
	if err != nil {
		return fmt.Errorf("error creating request: %w", err)
	}

	req.Header.Set(contentType, applicationJson)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("error sending message: %w", err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("error reading response body: %w", err)
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("telegram API returned status %d: %s", resp.StatusCode, string(body))
	}

	return nil
}
